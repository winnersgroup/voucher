var keystone = require('keystone'),
	Types = keystone.Field.Types,
	voucher = require('../voucher.js');

/**
 * Group Model
 * ==========
 */
var Group = new keystone.List('Group', {
	searchFields: 'name',
	track: {
		createdAt: true,
		updatedAt: true,
	},
	defaultSort: '-createdAt',
	defaultColumns: 'name|25%, product|20%, start|22%, end|22%, enabled|10%',
});

Group.add({
	name: { type: Types.Text, initial: true, index: true, required: true },
	enabled: { type: Types.Boolean, initial: true, index: true, default: true },
	product: { type: Types.Text, initial: true, index: true, required: true },
	start: { type: Types.Datetime, initial: true, index: true, required: true },
	end: { type: Types.Datetime, initial: true, index: true, required: true },
	capacity: { type: Types.Number, initial: true, index: false, hidden: true },
	qualifyFor: { type: Types.NumberArray, initial: true, index: false, hidden: false },
});

Group.relationship({ path: 'vouchers', ref: 'Voucher', refPath: 'group' });

Group.schema.pre('save', function(next) {
	// Saving isNew flag to re-use in post hook
	this.wasNew = this.isNew;
	next();
});

Group.schema.post('save', function(group) {
	// Spawn separate thread not to freeze the clients
	if (group.wasNew) setTimeout(() => voucher.generate(group._id, group.capacity));
});


/**
 * Registration
 */
Group.register();
