let keystone = require('keystone'),
	rest = require('../../rest.js'),
	voucher = require('../../voucher.js');

exports = module.exports = function(req, res) {

	let handler = new rest.Handler({
		req: req,
		res: res,
		list: null,
		allowed: {},
		valid: {},
		required: {},
		defaults: {},
		select: '',

		post: function() {
			 console.log('evaluating voucher');
			this.process(function(respond) {
				let opt = this.options;
				console.log('evaluating voucher', opt);
				if (!opt.input.code || !opt.input.product || !opt.input.user || !opt.input.currency)
					return respond([400, 0, 'No Code, Product, User or Currency provided']);
				var qualifyFor = opt.input.qualify ? opt.input.qualify : [];
				voucher.evaluate(opt.input.code, opt.input.product, opt.input.user, opt.input.currency, qualifyFor)
					.then(result => respond(null, result))
					.catch(err => respond(err, false));
			});

		}
	});

	handler.handle();
};
