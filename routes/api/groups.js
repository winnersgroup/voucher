var keystone = require('keystone'),
	rest = require('../../rest.js');

exports = module.exports = function(req, res) {

	var handler = new rest.Handler({
		req: req,
		res: res,
		list: keystone.list('Group'),
		allowed: {
			name: true,
			enabled: true,
			product: true,
			start: true,
			end: true,
			capacity: true
		},
		valid: {},
		required: {
			name: true,
			product: true,
			start: true,
			end: true
		},
		defaults: {
			enabled: true
		},
		select: 'name enabled product start end capacity',

		get: function() {

			var opt = this.options;

			if (opt.req.params.id) {
				this.read({ id: opt.req.params.id });
			}
			else {
				var args = {
					skip: opt.req.query.skip,
					limit: opt.req.query.limit,
					sort: opt.req.query.sort,
					count: opt.req.query.count !== 'false',
				};
				this.list(args);
			}

		},

		post: function() {

			this.create();

		},

		patch: function() {

			var opt = this.options;
			this.update({ id: opt.req.params.id });

		},

		delete: function() {

			var opt = this.options;
			this.delete({ id: opt.req.params.id });

		}
	});

	handler.handle();
};
