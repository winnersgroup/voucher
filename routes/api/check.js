let keystone = require('keystone'),
	rest = require('../../rest.js'),
	voucher = require('../../voucher.js');

exports = module.exports = function(req, res) {

	let handler = new rest.Handler({
		req: req,
		res: res,
		list: null,
		allowed: {},
		valid: {},
		required: {},
		defaults: {},
		select: '',

		post: function() {

			this.process(function(respond) {
				let opt = this.options;

				if (!opt.input.code || !opt.input.product)
					return respond([400, 0, 'No Code or Product provided']);

				voucher.check(opt.input.code, opt.input.product)
					.then(voucher => respond(null, voucher))
					.catch(err => respond(err, false));
			});

		}
	});

	handler.handle();
};
