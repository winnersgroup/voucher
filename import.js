module.exports = {
	import: function(filename, group) {
		let keystone = require('keystone');
		let csv = require('fast-csv');

		let vouchers = [];
		let now = new Date();

		console.log(new Date, 'Importing from ' + filename + '...');

		csv.fromPath(__dirname + '/' + filename, {
			delimiter: ';',
			ignoreEmpty: true,
			headers: true,
			trim: true,
		})
		.on('data', function(data) {
			vouchers.push({
				group: keystone.mongoose.Types.ObjectId(group),
				code: data.code.toUpperCase(),
				used: false,
				createdAt: now,
				updatedAt: now
			});
		})
		.on('end', function(lines) {
			console.log(new Date, 'Import completed: ' + lines + ' codes. Saving to the DB...');
			keystone.list('Voucher').model.collection.insertMany(
				vouchers,
				{bypassDocumentValidation: true},
				err => {
					if (err) {
						console.log(new Date, 'Saving Error', err);
					}
					else {
						console.log(new Date, 'Saving is done.');
					}
				}
			);
		});
	}
};
