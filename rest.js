var keystone = require('keystone');
var reversePopulate = require('mongoose-reverse-populate');

var Handler = function(options) {
	// override method and input for jsonp calls
	options.jsonp = options.req.query.callback;
	options.input = options.req.query.callback ? options.req.query : options.req.body;
	options.method = options.req.query.callback && options.req.query.method ? options.req.query.method : options.req.method;
	options.user = options.req.apiSession && options.req.apiSession.user;
	options.show = options.req.query.show !== 'false';
	// save options
	this.options = options;
};


Handler.prototype.notAllowed = function(text) {
	this.respond([405, 405, text || 'Method Not Allowed']);
};


Handler.prototype.forbidden = function(text) {
	this.respond([403, 403, text || 'Forbidden']);
};


Handler.prototype.notFound = function(text) {
	this.respond([404, 404, text || 'Not Found']);
};


Handler.prototype.badRequest = function(text) {
	this.respond([400, 400, text || 'Bad Request']);
};


Handler.prototype.send = function(err, data, meta) {
	this.respond(err, data, meta);
};


Handler.prototype.process = function(callback) {
	if (typeof callback === 'function') {
		var self = this;
		callback.call(this, function(err, data, meta) {
			self.respond.call(self, err, data, meta);
		});
	}
	else {
		this.respond();
	}
};


Handler.prototype.respond = function(err, data, meta) {
	var opt = this.options,
		out = {
			data: data,
			meta: {}
		};
	// error passed as array
	if (err instanceof Array) {
		out.meta = {
			status: err[0] || 500,
			error: {
				code: err[1] || 0,
				message: err[2] || '',
				original: (err[3] && err[3].message) ? err[3].message : err[3]
			}
		};
	}
	// error passed as object
	else if (err !== null && typeof err === 'object') {
		out.meta = {
			error: {
				code: err.code || 0,
				message: err.message || ''
			}
		};
	}
	// error passed as simple string
	else if (typeof err === 'string') {
		out.meta = {
			error: {
				code: 0,
				message: err
			}
		};
	}
	// normalize data value
	if (typeof out.data === 'undefined') out.data = null;
	// load meta properties
	if (meta !== null && typeof meta === 'object') {
		each(meta, function(value, key) {
			out.meta[key] = value;
		});
	}
	// determine success
	if (typeof out.meta.success !== 'boolean') out.meta.success = !out.meta.error;
	// set http status
	if (out.meta.status && !opt.jsonp) opt.res.status(out.meta.status);
	if (opt.jsonp) {
		// opt.res.jsonp(out);
		// native jsonp method fails in some cases
		opt.res.set('Content-Type', 'text/javascript; charset=utf-8');
		opt.res.send('/**/ typeof ' + opt.jsonp + ' === \'function\' && ' + opt.jsonp + '(' + JSON.stringify(out) + ');');
	}
	else {
		opt.res.json(out);
	}
};


Handler.prototype.handle = function(method) {
	var opt = this.options;
	if (!method) method = opt.method;
	if (!method) return this.badRequest('No Method to Handle');
	method = method.toLowerCase();
	if (typeof opt[method] === 'function') {
		return opt[method].call(this);
	}
	else {
		return this.respond([501, 501, 'Not Implemented']);
	}
};


Handler.prototype.create = function(arg) {
	var self = this,
		opt = this.options;
	if (!arg) arg = {};
	
	// access
	if (typeof arg.access === 'function' && !arg.access.call(this, opt.input))
		return this.respond([403, 213, 'Create Access Denied']);
	
	var save = function() {
		// validation
		var invalid = self.validate(opt.input, opt.allowed, opt.valid, opt.required, opt.defaults);
		if (invalid) return self.respond([400, 212, 'Field [' + invalid + '] Failed Validation']);
		
		var data = new opt.list.model(opt.input);
		// save user for auto createdBy field
		if (opt.user) data._req_user = opt.user;
		data.getUpdateHandler(opt.req).process(data, function(err) {
			if (err) return self.respond([500, 211, 'Cannot Create Item', err]);
			
			// after
			if (typeof arg.after === 'function') {
				arg.after.call(self, data);
			}
			self.respond(null, arg.show || opt.show ? data : { _id: data._id }, { status: 201 });
		});
	};
	
	// before
	if (typeof arg.before === 'function') {
		arg.before.call(this, opt.input, function(err) {
			if (err) return self.respond([400, 214, 'Cannot Create Item (Before)', err]);
			
			save();
		});
	}
	else {
		save();
	}
};


Handler.prototype.read = function(arg) {
 	var self = this,
		opt = this.options,
		query;
	if (!arg) arg = {};
	
	var select = arg.select || opt.select;
	
	if (arg.where) {
		query = opt.list.model.findOne().select(select);
		each(arg.where, function(value, key) {
			if (value instanceof Array) {
				query.where(key).in(value);
			}
			else {
				query.where(key, value);
			}
		});
	}
	else {
 		query = opt.list.model.findById(arg.id).select(select);
	}
	
	this.populate(opt.list.model, query, opt.populate, function(err, data) {
		if (err) return self.respond([500, 221, 'Cannot Read Item', err]);
		if (!data) return self.notFound();
		
		// convert data to array
		self.reverse([data], opt.reverse, function(err, data) {
			if (err) return self.respond([500, 221, 'Cannot Reverse Populate Item', err]);
			
			// extract data from array
			data = data[0];
			
			// access
			if (typeof arg.access === 'function' && !arg.access.call(self, data))
				return self.respond([403, 223, 'Read Access Denied']);
		
			// after
			if (typeof arg.after === 'function') {
				arg.after.call(self, data, function(err) {
					if (err) return self.respond([400, 222, 'Cannot Read Item (After)', err]);
					self.respond(null, data);
				});
			}
			else {
				self.respond(null, data);
			}
		});
	});
};


Handler.prototype.list = function(arg) {
	var self = this,
		opt = this.options;
	if (!arg) arg = {};
	
	// access
	if (typeof arg.access === 'function' && !arg.access.call(this))
		return this.respond([403, 233, 'List Access Denied']);
	
	var finish = function(count) {
		// applying params
		var skip = parseInt(arg.skip) || 0,
			limit = parseInt(arg.limit) || 10,
			sort = arg.sort || '',
			select = arg.select || opt.select;
		
		// hard limit
		if (limit > 100) limit = 100;
			
		var query = opt.list.model.find().skip(skip).limit(limit).select(select);
		if (sort) {
			var sorts = [];
			sort.split(',').forEach((item) => {
				sorts.push([item.replace('-', ''), (item.indexOf('-') === 0) ? -1 : 1]);
			});
			query.sort(sorts);
		}
			
		// where
		if (arg.where) opt.where = arg.where;
		if (opt.where) {
			each(opt.where, function(value, key) {
				if (value instanceof Array) {
					query.where(key).in(value);
				}
				else {
					query.where(key, value);
				}
			});
		}
		
		self.populate(opt.list.model, query, opt.populate, function(err, data) {
			if (err) return self.respond([500, 231, 'Cannot List Items', err]);
				
			self.reverse(data, opt.reverse, function(err, data) {
				if (err) return self.respond([500, 231, 'Cannot Reverse Populate Items', err]);
				
				// after
				if (typeof arg.after === 'function') {
					arg.after.call(self, data, function(err) {
						if (err) return self.respond([400, 232, 'Cannot List Items (After)', err]);
						self.respond(null, data, { total: count });
					});
				}
				else {
					self.respond(null, data, { total: count });
				}
			});
		});
		
	};
	
	if (arg.count === false) {
		finish(-1);
	}
	else {
		// getting total count
		var countQuery = opt.list.model.find();
	
		// where
		if (arg.where) opt.where = arg.where;
		if (opt.where) {
			each(opt.where, function(value, key) {
				if (value instanceof Array) {
					countQuery.where(key).in(value);
				}
				else {
					countQuery.where(key, value);
				}
			});
		}
	
		countQuery.count(function(err, count) {
			if (err) return self.respond([500, 234, 'Cannot Get Items Count', err]);
			if (!count) {
				
				// after
				if (typeof arg.after === 'function') {
					arg.after.call(self, [], function(err) {
						if (err) return self.respond([400, 232, 'Cannot List Items (After)', err]);
						self.respond(null, [], { total: 0 });
					});
				}
				else {
					self.respond(null, [], { total: 0 });
				}
			}
			else {
				finish(count);
			}
		});
	}
};


Handler.prototype.aggregate = function(arg) {
	var self = this,
		opt = this.options,
		pipeline = [],
		match = {},
		isMatchEmpty = true;
	if (!arg) arg = {};
	
	// access
	if (typeof arg.access === 'function' && !arg.access.call(this))
		return this.respond([403, 243, 'Aggregate Access Denied']);

	var finish = function(count) {
		// applying params
		var skip = parseInt(arg.skip) || 0,
			limit = parseInt(arg.limit) || 10,
			sort = arg.sort || '',
			select = arg.select || opt.select;

		// hard limit
		if (limit > 100) limit = 100;

		var query = opt.list.model.aggregate(pipeline.concat(arg.pipeline)).skip(skip).limit(limit).project(select);
		if (sort) {
			var sorts = [];
			sort.split(',').forEach((item) => {
				sorts.push([item.replace('-', ''), (item.indexOf('-') === 0) ? -1 : 1]);
			});
			query.sort(sorts);
		}
		
		query.exec(function(err, data) {
			if (err) return self.respond([500, 241, 'Cannot Aggregate Items', err]);
			
			// after
			if (typeof arg.after === 'function') {
				arg.after.call(self, data, function(err) {
					if (err) return self.respond([400, 242, 'Cannot Aggregate Items (After)', err]);
					self.respond(null, data, { total: count });
				});
			}
			else {
				self.respond(null, data, { total: count });
			}
		});
	};
	
	// where
	if (arg.where) opt.where = arg.where;
	if (opt.where) {
		each(opt.where, function(value, key) {
			if (value instanceof Array) {
				match[key] = { $in: value };
			}
			else {
				match[key] = value;
			}
			isMatchEmpty = false;
		});
	}
	if (!isMatchEmpty) pipeline.push({ $match: match });
	
	if (arg.count === false) {
		finish(-1);
	}
	else {
		// getting total count
		opt.list.model.aggregate(pipeline.concat(arg.pipeline).concat({ $group: { _id: null, count: { $sum: 1 } } }))
			.exec(function(err, data) {
				if (err) return self.respond([500, 244, 'Cannot Aggregate Items Count', err]);
				var count = data && data[0] && data[0].count || 0;
				if (!count) {
					
					// after
					if (typeof arg.after === 'function') {
						arg.after.call(self, [], function(err) {
							if (err) return self.respond([400, 242, 'Cannot Aggregate Items (After)', err]);
							self.respond(null, [], { total: 0 });
						});
					}
					else {
						self.respond(null, [], { total: 0 });
					}
				}
				else {
					finish(count);
				}
			});
	}
};


Handler.prototype.search = function(arg) {
	var self = this,
		opt = this.options;
	if (!arg) arg = {};
	
	if (!arg.query) return this.respond([400, 254, 'Search Query is Empty']);
	
	// access
	if (typeof arg.access === 'function' && !arg.access.call(this, arg.query))
		return this.respond([403, 253, 'Search Access Denied']);
	
	opt.list.model.textSearch(arg.query, arg.options, function(err, data) {
		if (err) return self.respond([500, 251, 'Cannot Search', err]);
		
		// after
		if (typeof arg.after === 'function') {
			arg.after.call(self, data, function(err) {
				if (err) return self.respond([400, 252, 'Cannot Search (After)', err]);
				self.respond(null, data);
			});
		}
		else {
			self.respond(null, data);
		}
	});
};


Handler.prototype.update = function(arg) {
	var self = this,
		opt = this.options;
	if (!arg) arg = {};
	
	var query = opt.list.model.findById(arg.id);
	query.exec(function(err, data) {
		if (err) return self.respond([500, 265, 'Cannot Find Item to Update', err]);
		if (!data) return self.notFound();
		
		// access
		if (typeof arg.access === 'function' && !arg.access.call(self, data))
			return self.respond([403, 263, 'Update Access Denied']);
		
		var save = function() {
			// validation
			var invalid = self.validate(opt.input, opt.allowed, opt.valid);
			if (invalid) return self.respond([400, 262, 'Field [' + invalid + '] Failed Validation']);
			
			each(opt.input, function(value, key) {
				data[key] = value;
			});
			
			// save user for auto updatedBy field
			if (opt.user) data._req_user = opt.user;
			
			// update date
			if (data.updatedAt) data.updatedAt = Date.now();
			
			data.getUpdateHandler(opt.req).process(data, function(err) {
				if (err) return self.respond([500, 261, 'Cannot Update Item', err]);
				
				// after
				if (typeof arg.after === 'function') {
					arg.after.call(self, data);
				}
				self.respond(null, arg.show || opt.show ? data : null);
			});
		};
		
		// before
		if (typeof arg.before === 'function') {
			arg.before.call(self, data, function(err) {
				if (err) return self.respond([400, 264, 'Cannot Update Item (Before)', err]);
				save();
			});
		}
		else {
			save();
		}
	});
};


Handler.prototype.delete = function(arg) {
 	var self = this,
		opt = this.options;
	if (!arg) arg = {};
	
	var query = opt.list.model.findById(arg.id);
	query.exec(function(err, data) {
		if (err) return self.respond([500, 272, 'Cannot Find Item to Delete', err]);
		// if (!data) return self.notFound();
		if (!data) return self.respond();	// return OK to be idempotent (for probably preiously deleted items)
		
		// access
		if (typeof arg.access === 'function' && !arg.access.call(self, data))
			return self.respond([403, 273, 'Delete Access Denied']);
		
		var remove = function() {
			// with children
			if (opt.req.query.children) {
				each(opt.req.query.children.split(','), function(child) {
					if (data[child] && (data[child] instanceof Array)) {
						var list = keystone.list(child);
						if (list && list.model) {
							list.model.remove({ _id: { $in: data[child] } }, function(err) {});
						}
					}
				});
			}
			
			data.remove(function(err) {
				if (err) return self.respond([500, 271, 'Cannot Delete Item', err]);
				
				// after
				if (typeof arg.after === 'function') {
					arg.after.call(self, data);
				}
				self.respond(null, arg.show || opt.show ? data : null);
			});
		};
		
		// before
		if (typeof arg.before === 'function') {
			arg.before.call(self, data, function(err) {
				if (err) return self.respond([400, 274, 'Cannot Delete Item (Before)', err]);
				remove();
			});
		}
		else {
			remove();
		}
	});
};


// mongoose populate helper, supports deep.links as well
Handler.prototype.populate = function(model, query, settings, callback) {
	var self = this,
		deep = {};
	for (var i in settings) {
		if (settings[i].active === false) continue;
		if (i.indexOf('.') === -1) {
			// simple populate
			query.populate({
				path: i,
				select: settings[i].select,
				match: settings[i].match
			});
		}
		else {
			// deep populate
			deep[i] = settings[i];
		}
	}
	query.exec(function(err, data) {
		if (err) {
			if (typeof callback === 'function') callback.call(self, err);
			return;
		}
		var steps = Object.keys(deep).length,
			finish = after(steps, function() {
				if (typeof callback === 'function') callback.call(self, null, data);
			});
		if (steps) {
			each(deep, function(options, path) {
				options.path = path;
				model.populate(data, options, function(err) {
					finish();
				});
				// using deepPopulate plugin
				// model.deepPopulate(data, path, function(err) {
				// 	finish();
				// });
			});
		}
		else {
			finish();
		}
	});
};


Handler.prototype.reverse = function(data, settings, callback) {
	if (!(settings instanceof Array)) {
		if (typeof settings !== 'object') {
			return callback(null, data);
		}
		else {
			settings = [settings];
		}
	}
	
	var finish = after(settings.length, function() {
		callback(null, data);
	});
	
	settings.forEach(function(setting) {
		if (setting.active === false) return finish();
		setting.modelArray = data;
		reversePopulate(setting, function(err, data) {
			if (err) return callback(err);
			finish();
		});
	})
};


// validation
Handler.prototype.validate = function(input, allowed, valid, required, defaults) {
	var _invalid = '';
	
	// allowed check
	for (var key in input) {
		if (!(key in allowed) || !(typeof allowed[key] === 'function' ? allowed[key].call(this, input[key], key) : allowed[key])) {
			delete input[key];
		}
	}
	
	// valid check
	for (var key in valid) {
		if (key in input) {
			// function
			if (typeof valid[key] === 'function') {
				var _result = valid[key].call(this, input[key], key);
				if (!_result) {
					_invalid = key;
					break;
				}
			}
			
			// validator
			else if ((typeof valid[key] === 'string') && (valid[key] in this.validators)) {
				var _result = this.validators[valid[key]].call(this, input, key);
				if (!_result) {
					_invalid = key;
					break;
				}
			}
			
			// regular expression
			else if (valid[key] instanceof RegExp) {
				if (!valid[key].test(input[key])) {
					_invalid = key;
					break;
				}
			}
			
			// falsey value
			else if (!valid[key]) {
				_invalid = key;
				break;
			}
		}
	}
	if (_invalid) return _invalid;
	
	// required check
	if (required) {
		for (var key in required) {
			if (key in input) continue;
			// function
			if (typeof required[key] === 'function') {
				var _result = required[key].call(this, key);
				if (_result) {
					_invalid = key;
					break;
				}
			}
		
			// falsey value
			else if (required[key]) {
				_invalid = key;
				break;
			}
		}
	}
	if (_invalid) return _invalid;
	
	// setting defaults
	if (defaults) {
		for (var key in defaults) {
			if (!(key in input)) {
				input[key] = typeof defaults[key] === 'function' ? defaults[key].call(this, key) : defaults[key];
			}
		}
	}
	
	return _invalid;
};


Handler.prototype.validators = {
	// validators
	isEmail: function(parent, key) {
		return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
			.test(parent[key]);
	},
	notEmptyString: function(parent, key) {
		return !!parent[key].trim();
	},
	notEmptyArray: function(parent, key) {
		return (parent[key] instanceof Array) && parent[key].length;
	},
	isArray: function(parent, key) {
		return (parent[key] instanceof Array);
	},
	isBoolean: function(parent, key) {
		return (typeof parent[key] === 'boolean');
	},
	// transformers
	ids: function(parent, key) {
		if (typeof parent[key] === 'string') {
			return parent[key] = parent[key] ? parent[key].split(',') : [];
		}
		else if (parent[key] instanceof Array) {
			var out = [];
			each(parent[key], function(item) {
				if (typeof item === 'string') {
					out.push(item);
				}
				else if (typeof item === 'object' && item._id) {
					out.push(item._id.toString());
				}
			});
			return parent[key] = out;
		}
		return parent[key];
	},
	toArray: function(parent, key) {
		if (typeof parent[key] === 'string')
			parent[key] = parent[key] ? parent[key].split(',') : [];
		return parent[key];
	},
	toBoolean: function(parent, key) {
		return parent[key] = !!parent[key];
	},
	toName: function(parent, key) {
		if (typeof parent[key] === 'string') {
			var parts = parent[key].split(' ');
			return parent[key] = {
				first: parts.shift(),
				last: parts.join(' ')
			};
		}
		else {
			return parent[key];
		}
	},
	stringify: function(parent, key) {
		try {
			parent[key] = JSON.stringify(parent[key]);
		}
		catch(e) {}
		return parent[key];
	},
	parse: function(parent, key) {
		try {
			parent[key] = JSON.parse(parent[key]);
		}
		catch(e) {}
		return parent[key];
	}
};


// utils
function each(obj, cb) {
	for (var key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key))
			cb.call(obj, obj[key], key, obj);
	}
	return obj;
};
// each({a: 1, b: 2}, function(value, key) {
// 	this[key] = ++value;
// });
// each({a: 1, b: 2}, (value, key, obj) => obj[key] = ++value);

function after(times, func) {
	return function() {
		if (--times < 1)
			return func.apply(this, arguments);
	};
};
// after(3, function() { console.log(arguments); });

function extend(obj) {
	for (var i=1; i<arguments.length; i++) {
		for (var key in arguments[i]) {
			if (Object.prototype.hasOwnProperty.call(arguments[i], key))
				obj[key] = arguments[i][key];
		}
	}
	return obj;
}
// extend({a: 1, b: 2}, {c: 3}, {d: 4, e: 5});


module.exports = {
	Handler: Handler
};
