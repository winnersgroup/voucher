/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	// Views
	app.get('/', routes.views.index);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

	// parse raw body
	var bodyParser = require('body-parser');
	app.use(bodyParser.raw({
		limit: '1mb',
		type: 'application/json'
	}));

	// API
	app.all('/api/*', function(req, res, next) {
		// no cache
		res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
		res.header('Expires', 'Thu, 01 Jan 1970 00:00:00 GMT');

		// jsonp
		if (req.query.callback) {
			next();
		}
		// cors
		else {
			if (req.method === 'OPTIONS') {
				res.writeHead(200, {
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
					'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept'
				});
				res.end();
			}
			else {
				res.setHeader('Access-Control-Allow-Origin', '*');
				next();
			}
		}
	});


	// open user routes
	app.post('/api/user/exists', routes.api.user);
	app.post('/api/user/login', routes.api.user);
	app.post('/api/user/register', routes.api.user);

	// closed user routes
	app.get('/api/user/logout', middleware.requireUserAPI, routes.api.user);
	app.get('/api/user/check', middleware.requireUserAPI, routes.api.user);
	app.post('/api/user/password', middleware.requireUserAPI, routes.api.user);

	// open routes
	app.post('/api/generate', routes.api.generate);
	app.post('/api/check', routes.api.check);
	app.post('/api/evaluate', routes.api.evaluate);
	app.post('/api/unassign', routes.api.unassign);
	app.all('/api/groups', routes.api.groups);


	// default routes

	// index
	app.all('/api/', function(req, res) {
		var out = {
			data: { message: 'Welcome to ' + keystone.get('name') + ' API' },
			meta: { success: true }
		};
		if (req.query.callback) {
			res.jsonp(out);
		}
		else {
			res.json(out);
		}
	});

	// 404
	app.all('/api/*', function(req, res) {
		var out = {
			data: null,
			meta: {
				error: { code: 11, message: 'Path Not Found' },
				success: false
			}
		};
		if (req.query.callback) {
			res.jsonp(out);
		}
		else {
			res.status(404);
			res.json(out);
		}
	});

};
