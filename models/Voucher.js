var keystone = require('keystone'),
	Types = keystone.Field.Types,
	shortid32 = require('shortid32');

/**
 * Voucher Model
 * ==========
 */
var Voucher = new keystone.List('Voucher', {
	searchFields: 'code',
	drilldown: 'group',
	track: {
		createdAt: true,
		updatedAt: true,
	},
	map: {
		name: 'code'
	},
	defaultSort: '-createdAt',
	defaultColumns: 'code|20%, group|25%, createdAt|22%, updatedAt|22%, used|10%',
});

Voucher.add({
	group: { type: Types.Relationship, ref: 'Group', initial: true, index: true, required: true },
	code: { type: Types.Text, initial: false, required: true, default: shortid32.generate, noedit: true },
	used: { type: Types.Boolean, initial: false, index: true, default: false },
	usedBy: { type: Types.Text,  noedit: true },
});

Voucher.schema.index({ code: 1 }, { unique: true });


/**
 * Registration
 */
Voucher.register();
