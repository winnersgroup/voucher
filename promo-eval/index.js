var request = require('request');
// var url = 'http://ec2-34-240-84-12.eu-west-1.compute.amazonaws.com:8090/node-promo-admin-controller/uploadTargettedUsersSingle';
// var urlRemove = 'http://ec2-34-240-84-12.eu-west-1.compute.amazonaws.com:8090/node-promo-admin-controller/removeTargettedUsersSingle';
var url = 'http://nodejs.prod.lycalotto.com:8090/node-promo-admin-controller/uploadTargettedUsers';
var urlRemove = 'http://nodejs.prod.lycalotto.com:8090/node-promo-admin-controller/removeTargettedUsersSingle';

var uploadUsers = function(voucher, userId, qualifyFor) {
	return new Promise((resolve, reject) => {
		const promises = [];
		let data = [];
		data.push(voucher.group.product);
		if (qualifyFor.length > 0) {
			data = data.concat(qualifyFor);
		}
		data.forEach((id) => {
			promises.push(upload(id, userId));
		});
		Promise.all(promises).then(function(success) {
			resolve(success);
		}, (err) => {
			reject(err);
		});
	});
};

var removeUsers = function(userId, promoIds) {
	return new Promise((resolve, reject) => {
		var promises = [];
		promoIds.forEach(function(id) {
			promises.push(remove(userId, id));
		});

		Promise.all(promises).then(function(success) {
			var arr = success.map(row => row.rowsRemoved);
			var rowsRemoved = arr.reduce((a, b) => {
				return a + b;
			}, 0);
			var response = {
				status: success[0].status,
				rowsRemoved,
			};
			console.log(response);
			resolve(response);
		}, (err) => {
			reject(err);
		});
	});
};


var remove = function(userId, promoId) {
	let data = {
		promoId: promoId,
		listType: 'USER_ID',
		list: userId,
	};

	return new Promise((resolve, reject) => {
		request({
			url: urlRemove,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			method: 'POST',
			json: data,
		}, (err, response, body) => {
			console.log(err, body);
			if (err) {
				reject(err);
			} else {
				resolve(body);
			}
		});
	});
};

/**
 * @description uploads user to promo eval engine
 * @param {Object} voucher   voucher used
 * @param {String} userId    user to apply promo to
 *
 * @return {Promise}
 */
var upload = function(id, userId) {
	let data = {
		promoId: id,
		listType: 'USER_ID',
		list: userId,
	};

	console.log('promo eval data ------>', data);

	console.log('promo eval url ------->', url);

	return new Promise((resolve, reject) => {
		request({
			url: url,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			method: 'POST',
			json: data,
		}, (err, response, body) => {
			console.log(err, body);
			if (err) {
				reject(err);
			} else {
				resolve(body);
			}
		});
	});
};

module.exports = {
	uploadUsers: uploadUsers,
	removeUsers: removeUsers,
};
