var keystone = require('keystone'),
	crypto = require('crypto'),
	rest = require('../../rest.js');


const emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const minPassword = 8;


exports = module.exports = function(req, res) {
	
	var handler = new rest.Handler({
		res: res,
		jsonp: !!req.query.callback,
		input: req.query.callback ? req.query : req.body,
		method: req.query.callback && req.query.method ? req.query.method : req.method,
		user: req.apiSession && req.apiSession.user,
	});
	
	var opt = handler.options;
	
	// "logout" requires token
	if (req.path === '/api/user/logout') {
		
		req.apiSession.remove(function(err) {
			if (err) return handler.respond([500, 101, 'Cannot Delete Session', err]);
			
			handler.respond();
		});
		
	}
	
	// "check" requires token
	else if (req.path === '/api/user/check') {
		
		handler.respond(null, {
			id: opt.user._id,
			name: opt.user.name,
			email: opt.user.email,
			domain: opt.user.domain,
		});
		
	}
	
	// "password" requires token
	else if (req.path === '/api/user/password') {
		
		var password = opt.input.password && opt.input.password.trim() || '',
			current = opt.input.current && opt.input.current.trim() || '';
		
		if (!password) {
			handler.respond([400, 131, 'Empty New Password']);
		}
		if (password !== opt.input.password) {
			handler.respond([400, 132, 'Password Starts or Ends With a Space']);
		}
		else if (password.length < 8) {
			handler.respond([400, 133, 'Password Shorter Than 8 Symbols']);
		}
		else {
			opt.user._.password.compare(current, function(err, isMatch) {
				if (err) return handler.respond([500, 134, 'Cannot Compare Passwords', err]);
				if (!isMatch) return handler.respond([400, 135, 'Incorrect Password']);
			
				opt.user.password = password;
				opt.user.save(function(err) {
					if (err) return handler.respond([500, 136, 'Cannot Update Password', err]);
				
					handler.respond();
				});
			});
		}
		
	}
	else if (req.path === '/api/user/exists') {
		
		var email = opt.input.email && opt.input.email.trim().toLowerCase();
		var domain = opt.input.domain && opt.input.domain.trim().toLowerCase();
		if (email && domain) {
		
			var User = keystone.list(keystone.get('user model'));
			User.model.findOne({ email: email, domain: domain }).exec(function(err, user) {
				if (err) return handler.respond([500, 109, 'Cannot Find Email or Domain', err]);
				
				handler.respond(null, { exists: !!user });
			});
			
		}
		else {
			handler.respond([400, 108, 'Empty Email or Domain']);
		}
		
	}
	else if (req.path === '/api/user/login') {
		
		var email = opt.input.email && opt.input.email.trim().toLowerCase(),
			domain = opt.input.domain && opt.input.domain.trim().toLowerCase(),
			password = opt.input.password && opt.input.password.trim();
		
		if (email && password && domain) {
	
			var User = keystone.list(keystone.get('user model'));
			User.model.findOne({ email: email, domain: domain }).exec(function(err, user) {
				if (err) return handler.respond([500, 103, 'Cannot Find Email or Domain', err]);
				if (!user) return handler.respond([401, 104, 'Incorrect Email or Domain']);
				
				user._.password.compare(password, function(err, isMatch) {
					if (err) return handler.respond([500, 105, 'Cannot Compare Passwords', err]);
					if (!isMatch) return handler.respond([401, 106, 'Incorrect Password']);
				
					createSession(user, req, function(err, session) {
						if (err) return handler.respond([500, 107, 'Cannot Create Session', err]);
					
						handler.respond(null, {
							token: session.token,
							id: user._id,
							name: user.name,
							email: user.email,
							domain: user.domain,
						});
					});
				});
			});
		
		}
		else {
			handler.respond([400, 102, 'Empty Email or Password or Domain']);
		}
		
	}
	else if (req.path === '/api/user/register') {
		
		var email = opt.input.email && opt.input.email.trim().toLowerCase(),
			domain = opt.input.domain && opt.input.domain.trim().toLowerCase(),
			password = opt.input.password && opt.input.password.trim(),
			name = opt.input.name;
	
		if (!email || !password || !domain || !name) {
			handler.respond([400, 121, 'Empty Email or Password or Domain or Name']);
		}							
		else if (password !== opt.input.password) {
			handler.respond([400, 122, 'Password Starts or Ends With a Space']);
		}
		else if (password.length < minPassword) {
			handler.respond([400, 123, 'Password Shorter Than ' + minPassword + ' Symbols']);
		}
		else if (!emailRegExp.test(email)) {
			handler.respond([400, 124, 'Incorrect Email']);
		}
		else {
		
			var User = keystone.list(keystone.get('user model'));
			User.model.findOne({ email: email, domain: domain }).exec(function(err, user) {
				if (user) return handler.respond([409, 125, 'Email Already Registered for this Domain']);
			
				// create user
				user = new User.model({
					email: email,
					password: password,
					domain: domain,
					isAdmin: false
				});
			
				// name can be string or object
				if (typeof name === 'string') {
					var parts = name.split(' ');
					user.name = {
						first: parts.shift(),
						last: parts.join(' ')
					};
				}
				else {
					user.name = {
						first: name.first,
						last: name.last
					};
				}
			
				user.save(function(err) {
					if (err) return handler.respond([500, 126, 'Cannot Create User', err]);
				
					createSession(user, req, function(err, session) {
						if (err) return handler.respond([500, 127, 'Cannot Create Session', err]);
					
						createPage(user, function(err, page) {
							if (err) return handler.respond([500, 128, 'Cannot Create Page', err]);
					
							createProject(user, page, function(err, project) {
								if (err) return handler.respond([500, 129, 'Cannot Create Project', err]);
					
								handler.respond(null, {
									token: session.token,
									id: user._id,
									name: user.name,
									email: user.email,
									domain: user.domain,
								});
							});
						});
					});
				
				});
				
			});
		
		}
		
	}
	else {
		
		handler.notFound();
		
	}
};


// utils

// make a token of user id and password with Keystone's cookie secret, adding random string
function makeToken(str) {
	var rnd = crypto.randomBytes(32).toString('hex'),
		rnd1 = rnd.substring(0, 16),
		rnd2 = rnd.substring(16);
	// hash using sha256
	return rnd1 + 
		crypto
		.createHmac('sha256', keystone.get('cookie secret'))
		.update(str)
		.digest('base64')
		.replace(/[^a-z\d]/ig, '')
		+ rnd2;
};

// create a session for user, saving ip and user-agent
function createSession(user, req, callback) {
	var Session = keystone.list('Session');
	var session = new Session.model({
		user: user,
		token: makeToken(user._id + user.password),
		agent: req.headers['user-agent'] || '',
		ip: req.ip || ''
	});
	session.save(function(err) {
		if (err) {
			callback(err);
		}
		else {
			callback(null, session);
		}
	});
};

// create a page for user
function createPage(user, callback) {
	var Page = keystone.list('Page');
	var page = new Page.model({
		domain: user.domain,
		name: 'Index of ' + user.domain,
		title: 'Index',
	});
	page._req_user = user;
	page.save(function(err) {
		if (err) {
			callback(err);
		}
		else {
			callback(null, page);
		}
	});
};

// create a project for user
function createProject(user, page, callback) {
	var Language = keystone.list('Language');
	Language.model.findOne({ code: 'en_GB' }, '_id', { lean: true }, function(err, language) {
		if (err) return callback(err);
		var Project = keystone.list('Project');
		var project = new Project.model({
			domain: user.domain,
			name: 'Project ' + user.domain,
			languages: [{
				language: language._id,
				isDefault: true,
				isEnabled: true,
			}],
			route: {
				path: '/',
				id: page._id,
				title: page.title,
				nodes: [],
			},
		});
		project._req_user = user;
		project.save(function(err) {
			if (err) return callback(err);
			callback(null, project);
		});
	});
};
