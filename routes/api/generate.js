let keystone = require('keystone'),
	rest = require('../../rest.js'),
	voucher = require('../../voucher.js');

exports = module.exports = function(req, res) {

	let handler = new rest.Handler({
		req: req,
		res: res,
		list: null,
		allowed: {},
		valid: {},
		required: {},
		defaults: {},
		select: '',

		post: function() {

			this.process(function(respond) {
				let opt = this.options;

				if (!opt.input.group || !opt.input.amount)
					return respond([400, 0, 'No Group or Amount provided']);

				respond(null, voucher.generate(opt.input.group, opt.input.amount));
			});

		}
	});

	handler.handle();
};
