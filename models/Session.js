var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * Session Model
 * ==========
 */

var Session = new keystone.List('Session', {
	track: {
		createdAt: true
	},
	nocreate: true
});

Session.add({
	user: { type: Types.Relationship, ref: 'User', required: true, initial: true, noedit: true },
	token: { type: String, required: true, initial: true, unique: true, noedit: true },
	agent: { type: String, noedit: true },
	ip: { type: String, noedit: true }
});


/**
 * Registration
 */

Session.defaultColumns = 'user, createdAt, ip, token';
Session.register();
