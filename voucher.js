let keystone = require('keystone'),
	shortid32 = require('shortid32'),
	request = require('request'),
	promo = require('./promo-eval');

module.exports = {

	generate: function(group, amount) {
		let vouchers = [];
		if (amount > 0) {
			console.log(new Date, 'Generating ' + amount + ' vouchers...');
			let now = new Date();
			for (let i = 0; i < amount; i++) {
				vouchers.push({
					group: (group instanceof keystone.mongoose.Types.ObjectId) ?
						group : keystone.mongoose.Types.ObjectId(group),
					code: shortid32.generate(),
					used: false,
					usedBy: '',
					createdAt: now,
					updatedAt: now
				});
			}
			console.log(new Date, 'Generating is done. Saving to the DB...');
			keystone.list('Voucher').model.collection.insertMany(
				vouchers, {
					bypassDocumentValidation: true
				},
				err => {
					if (err) {
						console.log(new Date, 'Saving Error', err);
					} else {
						console.log(new Date, 'Saving is done.');
					}
				}
			);
		} else {
			console.log(new Date, 'Zero capacity - no vouchers are generated.');
		}
		return vouchers;
	},

	check: function(code, product) {
		return new Promise((resolve, reject) => {
			keystone.list('Voucher').model.findOne()
				.where('code', code.toUpperCase())
				.where('used', false)
				.populate('group')
				.exec((err, voucher) => {
					if (err) {
						reject(err);
					} else if (voucher) {
						let now = new Date();
						if (!voucher.group.enabled) {
							reject(new Error('Voucher Group is disabled'));
						}
						// else if (voucher.group.product !== product) {
						// 	reject(new Error('Voucher Product does not match'));
						// }
						else if (voucher.group.start > now || voucher.group.end < now) {
							reject(new Error('Voucher Group is expired or not started yet'));
						} else {
							resolve(voucher);
						}
					} else {
						reject(new Error('Voucher not found'));
					}
				});
		});
	},

	evaluate (code, product, user, currency, qualifyFor) {
		return new Promise((resolve, reject) => {
			// special offer promo
			if (code === 'UPSELL' || code === 'FREELINE') {
				let voucher = {
					group: {
						product: product,
					},
				};
				var _qualifyFor = qualifyFor && qualifyFor.length ? qualifyFor : [];
				promo.uploadUsers(voucher, user, _qualifyFor).then((data) => {
					resolve(data);
				}, (err) => {
					reject(err);
				});
				return;
			}
			this.check(code, product)
				.then(voucher => {
					// if promo is not freeline
					if (parseInt(voucher.group.product, 10) !== 100) {
						console.log('extra products ----', qualifyFor)
						promo.uploadUsers(voucher, user, qualifyFor).then((data) => {
							voucher.used = true;
							voucher.usedBy = user;
							voucher.save(err => {
								if (err) {
									reject(err);
								} else {
									resolve(voucher);
								}
							});
						}, (err) => {
							reject(err);
						});
					} else { // if promo is freeline
						request({
							uri: process.env.PROMO_URL,
							method: 'POST',
							json: {
								extId: user,
								accCurrency: currency,
							},
						}, (err, response, body) => {
							if (err) {
								reject(err);
							} else if (response.statusCode !== 200) {
								reject(new Error('Promo service response code is not OK'));
							} else if (body.status !== 'SUCCESS') {
								reject(new Error('Promo service response status is not SUCCESS'));
							} else {
								voucher.used = true;
								voucher.save(err => {
									if (err) {
										reject(err);
									} else {
										resolve(voucher);
									}
								});
							}
						});
					}
				})
				.catch(err => reject(err));
		});
	}

};
