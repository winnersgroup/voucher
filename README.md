Voucher written in Node.js, based on Keystone.js with additional REST API layer.

Requirements:

- Node.js v6
- MongoDB v3

Config:

- create .env file in the root (you can use .env-sample as example)
- change COOKIE_SECRET to anything long and random enough
- adjust MONGO_URI for your database

Run:

- npm i
- node keystone
	- or "PORT=XXXX node keystone" for custom port setting (default is 3000)


Access Admin UI via http://localhost:3000/

API at http://localhost:3000/api

Apiary Docs at http://docs.nodevoucher.apiary.io/
