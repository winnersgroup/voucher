/**
 * This file contains the common middleware used by your routes.
 *
 * Extend or replace these functions as your application requires.
 *
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */
var _ = require('lodash');
var keystone = require('keystone');


/**
	Initialises the standard view locals

	The included layout depends on the navLinks array to generate
	the navigation in the header, you may wish to change this array
	or replace it with your own templates / logic.
*/
exports.initLocals = function (req, res, next) {
	res.locals.navLinks = [
		{ label: 'Home', key: 'home', href: '/' },
	];
	res.locals.user = req.user;
	next();
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/
exports.flashMessages = function (req, res, next) {
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error'),
	};
	res.locals.messages = _.some(flashMessages, function (msgs) { return msgs.length; }) ? flashMessages : false;
	next();
};


/**
	Prevents people from accessing protected pages when they're not signed in
 */
exports.requireUser = function (req, res, next) {
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}
};


/**
	Prevents people from accessing protected API when they're not signed in
 */

exports.requireUserAPI = function(req, res, next) {
	
	var data = null,
		meta = {
			error: null,
			success: false
		},
		respond = function() {
			var out = {
				data: data,
				meta: meta
			};
			if (meta.status && !req.query.callback) res.status(meta.status);
			if (req.query.callback) {
				res.jsonp(out);
			}
			else {
				res.json(out);
			}
		};
	
	var token = req.headers['authorization'];
	if (!token) token = req.body.token;
	if (!token) token = req.query.token;
	
	if (!token) {
		
		meta.error = {
			code: 111,
			message: 'Absent Token'
		};
		meta.status = 401;
		respond();
		
	}
	else {
		
		var Session = keystone.list('Session');
		Session.model.findOne({ token: token }).populate('user').exec(function(err, session) {
			if (err) {
				
				meta.error = {
					code: 112,
					message: 'Cannot Find Token'
				};
				meta.status = 500;
				respond();
				
			}
			else if (session && session.user) {
				
				req.apiSession = session;
				next();
				
			}
			else {
				
				meta.error = {
					code: 113,
					message: 'Incorrect Token'
				};
				meta.status = 401;
				respond();
				
			}
		});
		
	}
	
};
