let keystone = require('keystone'),
	rest = require('../../rest.js'),
	promoEval = require('../../promo-eval');

exports = module.exports = function(req, res) {

	let handler = new rest.Handler({
		req: req,
		res: res,
		list: null,
		allowed: {},
		valid: {},
		required: {},
		defaults: {},
		select: '',

		post: function() {

			this.process(function(respond) {
				let opt = this.options;

				console.log('new endpoint fucker', opt.input);
				if (!opt.input.promoIds || opt.input.promoIds.length === 0) {
					return respond([400, 0, 'No Promo ids provided']);
				}
				if (!opt.input.user) {
					return respond([400, 0, 'No user provided']);
				}
				promoEval.removeUsers(opt.input.user, opt.input.promoIds).then((success) => {
					respond(null, success);
				}, (err) => {
					respond(err, false);
				});

			});
		},
	});

	handler.handle();
};
